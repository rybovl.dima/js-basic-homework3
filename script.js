
// 1

let age = prompt("Введіть свій вік:");

if (age < 12) {
    alert("Ви є дитиною.");
} else if (age < 18) {
    alert("Ви є підлітком.");
} else {
    alert("Ви є дорослим.");
}

// 2

let input = prompt("Введіть число:");

if (isNaN(input)) {
    alert("Введено не число.");
} else {
    alert("Введено число.");
}

let month = prompt("Введіть місяць року (українською мовою маленькими літерами):");

switch (month) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log("У цьому місяці 31 день.");
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log("У цьому місяці 30 днів.");
        break;
    case "лютий":
        console.log("У цьому місяці 28 або 29 днів (залежно від високосного року).");
        break;
    default:
        console.log("Некоректно введений місяць.");
}